"use strict";

const functions = require("firebase-functions"); // Cloud Functions for Firebase library
const DialogflowApp = require("actions-on-google").DialogflowApp; // Google Assistant helper library
var admin = require("firebase-admin");

// Access to the database
admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: "https://aui-bot.firebaseio.com"
});

// Variables initializations
var db = admin.firestore();
var social_stories = db.collection("social_stories");

var last_story = {};
var index_question = 0;

// Webhook
exports.dialogflowFirebaseFulfillment = functions.https.onRequest(
  (request, response) => {
    console.log(
      "Dialogflow Request headers: " + JSON.stringify(request.headers)
    );
    console.log("Dialogflow Request body: " + JSON.stringify(request.body));
    if (request.body.result) {
      processV2Request(request, response);
    } else if (request.body.queryResult) {
      processV2Request(request, response);
    } else {
      console.log("Invalid Request");
      return response
        .status(400)
        .end("Invalid Webhook Request (expecting v1 or v2 webhook request)");
    }
  }
);
/*
* Function to handle v2 webhook requests from Dialogflow
*/
function processV2Request(request, response) {
  // An action is a string used to identify what needs to be done in fulfillment
  let action = request.body.queryResult.action
    ? request.body.queryResult.action
    : "default";
  // Parameters are any entites that Dialogflow has extracted from the request.
  let parameters = request.body.queryResult.parameters || {}; // https://dialogflow.com/docs/actions-and-parameters
  // Contexts are objects used to track and store conversation state
  let inputContexts = request.body.queryResult.contexts; // https://dialogflow.com/docs/contexts
  // Get the request source (Google Assistant, Slack, API, etc)
  let requestSource = request.body.originalDetectIntentRequest
    ? request.body.originalDetectIntentRequest.source
    : undefined;
  // Get the session ID to differentiate calls from different users
  let session = request.body.session ? request.body.session : undefined;
  // Create handlers for Dialogflow actions as well as a 'default' handler
  const actionHandlers = {
    // The default welcome intent has been matched, welcome the user (https://dialogflow.com/docs/events#default_welcome_intent)
    "input.welcome": () => {
      sendResponse("Hello, Welcome to my Dialogflow agent!"); // Send simple response to user
    },

    "story.selection": () => {
      var available_stories = [];
      social_stories
        .get()
        .then(snapshot => {
          snapshot.forEach(doc => {
            available_stories.push(doc.id);
          });
          var question = "What story do you want me to read?";
          sendMultipleMessages([createQuickReply(question, available_stories)]);
        })
        .catch(err => {
          console.log("Error getting documents", err);
        });
    },

    "story.steps": () => {
      var docname = parameters.socialStories_name
        ? parameters.socialStories_name
        : undefined;
      var stepslist = [];
      if (docname) {
        social_stories
          .doc(docname)
          .get()
          .then(doc => {
            last_story = doc.data();
            var data = last_story.steps;
            data.forEach(step => {
              stepslist.push(
                createCardReply(
                  step.title,
                  step.description,
                  step.imageUri,
                  step.website
                )
              );
            });
            sendMultipleMessages(stepslist);
          })
          .catch(err => {
            sendResponse("Error getting documents");
          });
      } else {
        var data = last_story.steps;
        data.forEach(step => {
          stepslist.push(
            createCardReply(
              step.title,
              step.description,
              step.imageUri,
              step.website
            )
          );
        });
        sendMultipleMessages(stepslist);
      }
    },

    "story.reading": () => {
      index_question = 0;
      var docname = parameters.socialStories_name;
      social_stories
        .doc(docname)
        .get()
        .then(doc => {
          if (!doc.exists) {
            console.log("No such document!");
          } else {
            last_story = doc.data();
            var question = "Do you want to try answering my questions?";
            var possibleAnswers = ["Yes", "No"];
            sendMultipleMessages([
              createTextReply(last_story.story),
              createQuickReply(question, possibleAnswers)
            ]);
          }
        })
        .catch(err => {
          console.log("Error getting documents", err);
        });
    },

    "story_reading.story_reading-yes": () => {
      sendQuestion();
    },

    "question.next": () => {
      sendQuestion();
    },

    "story.answerCheck": () => {
      var userAnswer = parameters.answer;
      var answer = last_story.quickReplies[index_question].goodAnswer;
      if (answer) {
        index_question++;
        var chatResponse = "";
        if (answer == userAnswer) {
          chatResponse = "Your answer is correct. Congratulations! ;)";
        } else {
          chatResponse =
            "Oh no.. The answer was " +
            answer +
            " you'll do better next time, don't worry! :)";
        }
        if (!last_story.quickReplies[index_question]) {
          sendMultipleMessages([
            createTextReply(chatResponse),
            createTextReply("I have no more questions.")
          ]);
        } else {
          sendMultipleMessages([
            createQuickReply(chatResponse, ["Next question"])
          ]);
        }
      } else {
        sendResponse("No question available.");
      }
    },

    "story.video": () => {
      var docname = parameters.socialStories_name
        ? parameters.socialStories_name
        : undefined;
      if (!docname) {
        sendResponse("Here is the video ! :) " + "\n" + last_story.video_link);
      } else {
        social_stories
          .doc(docname)
          .get()
          .then(doc => {
            if (!doc.exists) {
              console.log("No such document!");
            } else {
              last_story = doc.data();
              sendResponse(
                "Here is the video ! :) " + "\n" + last_story.video_link
              );
            }
          })
          .catch(err => {
            console.log("Error getting documents", err);
          });
      }
    },

    "story.tools": () => {
      var docname = parameters.socialStories_name
        ? parameters.socialStories_name
        : undefined;
      if (!docname) {
        sendResponse(
          "You will need those tools:  " + "\n" + last_story.needed_tools.join()
        );
      } else {
        social_stories
          .doc(docname)
          .get()
          .then(doc => {
            if (!doc.exists) {
              console.log("No such document!");
            } else {
              last_story = doc.data();
              sendResponse(
                "You will need those tools: " +
                  "\n" +
                  last_story.needed_tools.join()
              );
            }
          })
          .catch(err => {
            console.log("Error getting documents", err);
          });
      }
    },

    // The default fallback intent has been matched, try to recover (https://dialogflow.com/docs/intents#fallback_intents)
    "input.unknown": () => {
      // Use the Actions on Google lib to respond to Google requests; for other requests use JSON
      sendResponse("I'm having trouble, can you try that again?"); // Send simple response to user
    },
    // Default handler for unknown or undefined actions
    default: () => {
      let responseToUser = {
        //fulfillmentMessages: richResponsesV2, // Optional, uncomment to enable
        //outputContexts: [{ 'name': `${session}/contexts/weather`, 'lifespanCount': 2, 'parameters': {'city': 'Rome'} }], // Optional, uncomment to enable
        fulfillmentText:
          "This is from Dialogflow's Cloud Functions for Firebase editor! :-)" // displayed response
      };
      sendResponse(responseToUser);
    }
  };

  // If undefined or unknown action use the default handler
  if (!actionHandlers[action]) {
    action = "default";
  }
  // Run the proper handler function to handle the request from Dialogflow
  actionHandlers[action]();

  function sendQuestion() {
    var lastElement = last_story.quickReplies[index_question];
    if (lastElement) {
      var question = lastElement.question;
      var possibleAnswers = lastElement.possibleAnswers.split(";");
      sendMultipleMessages([createQuickReply(question, possibleAnswers)]);
    }
  }

  function getAllSteps(docname, cb) {
    var stepslist = [];
    social_stories
      .doc(docname)
      .collection("steps")
      .get()
      .then(snapshot => {
        snapshot.forEach(doc => {
          stepslist.push(doc.data());
        });
        cb(null, stepslist);
      })
      .catch(err => {
        console.log("Error getting documents", err);
        cb(err, null);
      });
  }

  function createCardReply(title, description, url, website) {
    var reply = {
      platform: "FACEBOOK",
      card: {
        title: title,
        subtitle: description,
        imageUri: url,
        buttons: [
          {
            text: "More infos",
            postback: website
          }
        ]
      }
    };
    return reply;
  }

  function createImgReply(url) {
    var reply = {
      platform: "FACEBOOK",
      image: {
        imageUri: url
      }
    };
    return reply;
  }

  function createTextReply(content) {
    var reply = {
      platform: "FACEBOOK",
      text: {
        text: [content]
      }
    };
    return reply;
  }

  function createQuickReply(question, responseList) {
    var reply = {
      platform: "FACEBOOK",
      quickReplies: {
        title: question,
        quickReplies: responseList
      }
    };
    return reply;
  }

  function sendMultipleMessages(questions_responses) {
    let responseToUser = {
      fulfillmentMessages: questions_responses
    };
    sendResponse(responseToUser);
  }

  // Function to send correctly formatted responses to Dialogflow which are then sent to the user
  function sendResponse(responseToUser) {
    // if the response is a string send it as a response to the user
    if (typeof responseToUser === "string") {
      let responseJson = {
        fulfillmentText: responseToUser
      }; // displayed response
      response.json(responseJson); // Send response to Dialogflow
    } else {
      // If the response to the user includes rich responses or contexts send them to Dialogflow
      let responseJson = {};
      // Define the text response
      responseJson.fulfillmentText = responseToUser.fulfillmentText;
      // Optional: add rich messages for integrations (https://dialogflow.com/docs/rich-messages)
      if (responseToUser.fulfillmentMessages) {
        responseJson.fulfillmentMessages = responseToUser.fulfillmentMessages;
      }
      // Optional: add contexts (https://dialogflow.com/docs/contexts)
      if (responseToUser.outputContexts) {
        responseJson.outputContexts = responseToUser.outputContexts;
      }

      if (responseToUser.followupEventInput) {
        responseJson.followupEventInput = responseToUser.followupEventInput;
      }
      // Send the response to Dialogflow
      console.log("Response to Dialogflow: " + JSON.stringify(responseJson));
      response.json(responseJson);
    }
  }

  const richResponseV2Card = {
    title: "Title: this is a title",
    subtitle:
      "This is an subtitle.  Text can include unicode characters including emoji 📱.",
    imageUri:
      "https://developers.google.com/actions/images/badges/XPM_BADGING_GoogleAssistant_VER.png",
    buttons: [
      {
        text: "This is a button",
        postback: "https://assistant.google.com/"
      }
    ]
  };
}
