function testFirebase() {
    try {
        let app = firebase.app();
        let features = ['auth', 'database', 'messaging', 'storage', 'firestore'].filter(feature => typeof app[feature] === 'function');
        document.getElementById('load').innerHTML = `Firebase SDK loaded with ${features.join(', ')}`;
    } catch (e) {
        console.error(e);
        document.getElementById('load').innerHTML = 'Error loading the Firebase SDK, check the console.';
    }
}

function createEntity(title, synonyms = []) {
    var newEntity = {
        entities: [
            {
                value: title,
                synonyms: [
                    title
                ]
            }
        ]
    };

    newEntity.entities[0].synonyms.push(synonyms);

    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();

        xhr.open(
            "POST",
            'https://dialogflow.googleapis.com/v2beta1/projects/aui-bot/agent/entityTypes/8dbda7f0-022b-4d6c-9f3c-0af653cf5181/entities:batchCreate',
            true
        );
    
        //Send the proper header information along with the request
        xhr.setRequestHeader("Authorization", "Bearer ya29.c.El9pBXxOXvXOP5jy7ZRdslY98WhONGpTTfpZBLXKtKlzz-8GAJ9HPN--KP0sd9rTX0K4dAdiXiahiX3u8fA_vmGwLHDZ1M3C2NG4Bs1dbyzeaq1kz_APaLZB544geIrCBQ");
        xhr.setRequestHeader("Content-type", "application/json");
    
        xhr.onload = () => resolve(xhr.responseText);
        xhr.onerror = () => reject(xhr.statusText);
    
        xhr.send(JSON.stringify(newEntity));
    }); 
}

function createStory(title, data) {
    // TODO add load indicator

    const db = firebase.firestore();
    const snackbarContainer = document.querySelector('#error-snackbar');

    db.collection("social_stories").doc(title).set(data)
    .then(function () {
        console.log("Social story added to database!");

        createEntity(title)
        .then(function (response) { // objet Operation renvoyé par Dialogflow
            response = JSON.parse(response);

            if (response.error) {
                throw response.error.message;
            } else {
                console.log("Entiy successfully created: " + title);            
                window.location.href = "/";
            }            
        })
        .catch(function (error) {
            console.log("Entity not created: " + error);

            // suppression dans la db puis affichage erreur
            db.collection("social_stories").doc(title).delete()
            .then(function () {
                var data = {
                    message: 'Entity creation failed.',
                    timeout: 20000,
                    actionHandler: createStory,
                    actionText: 'Retry'
                };
            })
            .catch(function () {
                var data = {
                    message: 'Database error occurred.',
                    timeout: 20000,
                    actionHandler: createStory, // TODO logout
                    actionText: 'Logout'
                };
            })
            .then(function () {
                snackbarContainer.MaterialSnackbar.showSnackbar(data);
            });     
        });
    })
    .catch(function (error) {
        console.error("Error adding story: ", error);

        var data = {
            message: 'Story creation failed.',
            timeout: 20000,
            actionHandler: createStory,
            actionText: 'Retry'
        };
        snackbarContainer.MaterialSnackbar.showSnackbar(data);
    });
}

var tools = [];
var replies = [];
var steps = [];

const listTools = document.getElementById('listTools');
const listReplies = document.getElementById('listReplies');
const listSteps = document.getElementById('listSteps');

function refreshTools() {
    listTools.innerHTML = tools.reduce((acc, tool) => {
        return acc + `
        <span class="mdl-chip">
            <span class="mdl-chip__text">${tool}</span>
        </span>`;
    }, "");
}

function refreshReplies() {
    listReplies.innerHTML = replies.reduce((acc, reply) => {
        return acc + `
        <li class="mdl-list__item mdl-list__item--two-line">
            <span class="mdl-list__item-primary-content">
                ${reply.question}
                <span class="mdl-list__item-sub-title">
                ${"> " + reply.possibleAnswers.split(';').join(', ')}                    
                </span>
            </span>
        </li>
        `;
    }, "");
}

function refreshSteps() {
    listSteps.innerHTML = steps.reduce((acc, step) => {
        return acc + `
        <li class="mdl-list__item mdl-list__item--three-line">
            <span class="mdl-list__item-primary-content">
                <span>
                    <a href="${step.website}">${step.title}</a>
                </span>
                <span class="mdl-list__item-text-body">
                ${step.description}                    
                </span>
            </span>
            <span class="mdl-list__item-secondary-content">
                <a class="mdl-list__item-secondary-action" href="${step.imageUri}"><i class="material-icons">image</i></a>
            </span>
        </li>
        `;
    }, "");
}

document.addEventListener('DOMContentLoaded', function() {

testFirebase();

document.getElementById('saveStory').addEventListener('click', () => {
    var title = document.getElementById('title').value;
    var schedule = document.getElementById('schedule').value;
    var content = document.getElementById('content').value;
    var video = document.getElementById('video').value;
    var warnings = document.getElementById('warnings').value;

    if (title != "" && schedule != "" && content != "")
    createStory(title, {
        name: title,
        needed_tools: tools,
        quickReplies: replies,
        schedule: schedule,
        steps: steps,
        story: content,
        video_link: video,
        warnings: warnings
    });            
});

const storiesNav = document.getElementById('storiesNav');

var db = firebase.firestore();

db.collection("social_stories").get().then((querySnapshot) => {
    querySnapshot.forEach((doc) => {
    console.log(`${doc.id} => ${doc.data()}`);

    var storyLink = document.createElement('a');
    storyLink.classList.add("mdl-navigation__link");
    storyLink.innerHTML = doc.id;

    storiesNav.appendChild(storyLink);
    });
});

var dialogTool = document.querySelector('#dialogTool');
var showDialogToolBtn = document.querySelector('#show-dialogTool');
if (! dialogTool.showModal) {
    dialogPolyfill.registerDialog(dialogTool);
}
showDialogToolBtn.addEventListener('click', function() {
    dialogTool.showModal();
});
dialogTool.querySelector('.close').addEventListener('click', function() {
    dialogTool.close();
});
dialogTool.querySelector('.addTool').addEventListener('click', function() {
    var newToolName = dialogTool.querySelector('#newTool').value;
    if (newToolName != "") {
        tools.push(newToolName);
        refreshTools();
        dialogTool.close();
    }
});

var dialogReply = document.querySelector('#dialogReply');
var showDialogReplyBtn = document.querySelector('#show-dialogReply');
if (! dialogReply.showModal) {
    dialogPolyfill.registerDialog(dialogReply);
}
showDialogReplyBtn.addEventListener('click', function() {
    dialogReply.showModal();
});
dialogReply.querySelector('.close').addEventListener('click', function() {
    dialogReply.close();
});
dialogReply.querySelector('.addReply').addEventListener('click', function() {
    var newQuestion = dialogReply.querySelector('#newQuestion').value;
    var newAnswer = dialogReply.querySelector('#newAnswer').value;
    var newReplies = dialogReply.querySelector('#newReplies').value;
    if (newQuestion != "" && newAnswer != "" & newReplies != "") {
        replies.push({
            goodAnswer: newAnswer,
            possibleAnswers: newAnswer + ";" + newReplies,
            question: newQuestion
        });
        refreshReplies();
        dialogReply.close();
    }
});

var dialogStep = document.querySelector('#dialogStep');
var showDialogStepBtn = document.querySelector('#show-dialogStep');
if (! dialogStep.showModal) {
    dialogPolyfill.registerDialog(dialogStep);
}
showDialogStepBtn.addEventListener('click', function() {
    dialogStep.showModal();
});
dialogStep.querySelector('.close').addEventListener('click', function() {
    dialogStep.close();
});
dialogStep.querySelector('.addStep').addEventListener('click', function() {
    var newTitle = dialogStep.querySelector('#newTitle').value;
    var newLink = dialogStep.querySelector('#newLink').value;
    var newDescription = dialogStep.querySelector('#newDescription').value;
    var newImg = dialogStep.querySelector('#newImg').value;
    if (newTitle != "" && newLink != "" & newDescription != "" && newImg != "") {
        steps.push({
            description: newDescription,
            imageUri: newImg,
            title: newTitle,
            website: newLink
        });
        refreshSteps();
        dialogStep.close();
    }
});

});