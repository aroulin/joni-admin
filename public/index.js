function testFirebase() {
    try {
        let app = firebase.app();
        let features = ['auth', 'database', 'messaging', 'storage', 'firestore'].filter(feature => typeof app[feature] === 'function');
        document.getElementById('load').innerHTML = `Firebase SDK loaded with ${features.join(', ')}`;
    } catch (e) {
        console.error(e);
        document.getElementById('load').innerHTML = 'Error loading the Firebase SDK, check the console.';
    }
}

function displayStoriesList() {
    const storiesList = document.getElementById('stories');
    const storiesNav = document.getElementById('storiesNav');

    storiesList.innerHTML = '<div id="storyProgress" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>';

    // Initialize Cloud Firestore through Firebase
    var db = firebase.firestore();

    db.collection("social_stories").get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            console.log(`${doc.id} => ${doc.data()}`);

            var storyNode = document.createElement("div");
            storyNode.classList.add("mdl-card", "mdl-shadow--2dp");

            repliesList = doc.data().quickReplies.reduce((replies, reply) => {
                return replies + `
                <li class="mdl-list__item mdl-list__item--two-line">
                    <span class="mdl-list__item-primary-content">
                        ${reply.question}
                        <span class="mdl-list__item-sub-title">
                        ${reply.possibleAnswers.split(';').join(', ')}                    
                        </span>
                    </span>
                </li>
                `;
            }, "");

            stepList = doc.data().steps.reduce((steps, step) => {
                return steps + `
                <li class="mdl-list__item mdl-list__item--three-line">
                    <span class="mdl-list__item-primary-content">
                        <span>
                            <a href="${step.website}">${step.title}</a>
                        </span>
                        <span class="mdl-list__item-text-body">
                        ${step.description}                    
                        </span>
                    </span>
                    <span class="mdl-list__item-secondary-content">
                        <a class="mdl-list__item-secondary-action" href="${step.imageUri}"><i class="material-icons">image</i></a>
                    </span>
                </li>
                `;
            }, "");

            storyNode.innerHTML =
            `
            <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">${doc.id}</h2>
            </div>
            <div class="mdl-card__supporting-text">
                <ul class='mdl-list'>
                    <li class="mdl-list__item">
                        <span class="mdl-list__item-primary-content">
                        <i class="material-icons mdl-list__item-icon">build</i>
                        ${doc.data().needed_tools.reduce((tools, tool) => {
                            return tools + `
                            <span class="mdl-chip">
                                <span class="mdl-chip__text">${tool}</span>
                            </span>`;
                        }, "")}
                        </span>
                    </li>
                    <li class="mdl-list__item">
                        <span class="mdl-list__item-primary-content">
                        <i class="material-icons mdl-list__item-icon">alarm</i>
                        ${doc.data().schedule}
                        </span>
                    </li>
                    <li class="mdl-list__item">
                        <span class="mdl-list__item-primary-content">
                        <i class="material-icons mdl-list__item-icon">comment</i>
                        ${doc.data().story}
                        </span>
                    </li>
                    <li class="mdl-list__item">
                        <span class="mdl-list__item-primary-content">
                        <i class="material-icons mdl-list__item-icon">movie</i>
                        <a href="${doc.data().video_link}">${doc.data().video_link}</a>
                        </span>
                    </li>
                    <li class="mdl-list__item">
                        <span class="mdl-list__item-primary-content">
                        <i class="material-icons mdl-list__item-icon">warning</i>
                        ${doc.data().warnings}
                        </span>
                    </li>
                </ul>
                <h3><span class="mdl-badge" data-badge="${doc.data().steps.length}">Steps</span></h3>
                <ul class='mdl-list'>
                    ${stepList}
                </ul>
                <h3><span class="mdl-badge" data-badge="${doc.data().quickReplies.length}">Questions</span></h3>
                <ul class='mdl-list'>
                    ${repliesList}
                </ul>
            </div>
            <div class="mdl-card__menu">
                <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-button--accent">
                <i class="material-icons">mode_edit</i>
                </button>
            </div>
            `;
            
            storiesList.appendChild(storyNode);
            
            var storyLink = document.createElement('a');
            storyLink.classList.add("mdl-navigation__link");
            storyLink.innerHTML = doc.id;
            
            storiesNav.appendChild(storyLink);
        });
        
        document.getElementById("storyProgress").remove();
    });
}

function displayLogin() {
    // FirebaseUI config.
    var uiConfig = {
        signInSuccessUrl: '/',
        signInOptions: [
            // Leave the lines as is for the providers you want to offer your users.
            {
                // Google provider must be enabled in Firebase Console to support one-tap sign-up.
                provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                authMethod: 'https://accounts.google.com',
                clientId: '365956987227-hh5i7hrq1emvu35mda6gnitis3glftk9.apps.googleusercontent.com'
            },
            //firebase.auth.FacebookAuthProvider.PROVIDER_ID,
            //firebase.auth.TwitterAuthProvider.PROVIDER_ID,
            //firebase.auth.GithubAuthProvider.PROVIDER_ID,
            //firebase.auth.PhoneAuthProvider.PROVIDER_ID,
            firebase.auth.EmailAuthProvider.PROVIDER_ID
        ],
        // Terms of service url.
        tosUrl: '<your-tos-url>',
        credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO
    };

    // Initialize the FirebaseUI Widget using Firebase.
    var ui = new firebaseui.auth.AuthUI(firebase.auth());
    // The start method will wait until the DOM is loaded.
    ui.start('#firebaseui-auth-container', uiConfig);
}

document.addEventListener('DOMContentLoaded', function() {

    testFirebase();

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            // User is signed in.
            displayStoriesList();
        } else {
            // User is signed out.
            displayLogin();
        }
    }, function(error) {
        console.log(error);
    });
});