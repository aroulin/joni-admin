# Joni - Admin

Interface to manage the content used by a Facebook Messenger bot.

## Development

First make sure all needed Node.js packages are installed.
Then use `firebase serve` in the parent folder to get a page accessible at <localhost:5000> by default.
To deploy your work in production, use `firebase deploy`.

## About

This project is made for an Advanced User Interface course in Politecnico di Milano.
